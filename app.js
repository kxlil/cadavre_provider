const express = require('express')
const cors = require('cors')
const axios = require('axios').default
const app = express()
const dotenv = require('dotenv')
const logger = require('morgan')

// Configuration
dotenv.config()

// Middleware
app.use(cors())
app.use(logger('tiny'))

// Variable d'environnement
const port = process.env.PORT || 4000
const host = process.env.HOST || 'localhost'
const instanceId = process.env.INSTANCE_ID
const providerUrl = process.env.PROVIDER_URL
const registerUrls = process.env.REGISTER_URLS.split(' ') 
console.log(registerUrls)

// liste verbs/adjectives/subjects
const subjects = ['kalil', 'dog', 'doctor', 'daddy']
const verbs = ['eat', 'run', 'buy', 'broke']
const adjectives = ['good', 'happy', 'tired', 'orange']

// enregistrement des providers auprès du register
register()

// Get a subject
app.get('/subject', (req, res) => {
    const index = Math.floor(Math.random() * subjects.length)
    res.json({
        value: subjects[index],
        instanceId
    })
})

//  Get a verb
app.get('/verb', (req, res) => {
    const index = Math.floor(Math.random() * verbs.length)
    res.json({
        value: verbs[index],
        instanceId
    })
})

// Get a adjective
app.get('/adjective', (req, res) => {
    const index = Math.floor(Math.random() * adjectives.length)
    res.json({
        value: adjectives[index],
        instanceId
    })
})

const server = app.listen(port, host, () => {
    console.log(`App listening at https://${host}:${port}`)
})

// Fonction d'enregistrement au près des registers
async function  register() {
    for(const registerUrl of registerUrls) {
        await axios.put(`${registerUrl}/providers`, { url: providerUrl, type: "VERB"}).catch(err => console.log(err.message))
        await axios.put(`${registerUrl}/providers`, { url: providerUrl, type: "SUBJECT"}).catch(err => console.log(err.message))
        await axios.put(`${registerUrl}/providers`, { url: providerUrl, type: "ADJECTIVE"}).catch(err => console.log(err.message))
    }
}

// Gracefull shutdown
function gracefull() {
    console.info('signal received.');
    console.log('Closing http server.');
    server.close(() => {
        console.log('Http server closed.');
        process.exit(0);
    });
}

process.on('SIGTERM', gracefull)
process.on('SIGINT', gracefull)